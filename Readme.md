# Branch Creator
Very small app, which create git-branches.

## Why should I use it? 
Create branches in git is indeed a very simple task.
How ever with branch creator you get a list of your jira-issues.
Bases on this information a branchname will be created.
Additional you can define target branches. 
The tool will find then the merge-base of these commits and start the new branch from this point.

## How can I used it?
You need an jre 8 with javafx. I think the easiest way to get it is [ZuluFx](https://www.azul.com/downloads/zulu/zulufx/). 
If you use openjdk make sure javaFx is installed. Java 9 isn't supported.
1. Download the latest release and unzip.
2. Adjust the `application.properties` to your needs.
3. Make sure your java-version supports the ssl-certificate of you jira-installation.
`keytool -import -trustcacerts -keystore ../jre/lib/security/cacerts -storepass changeit -alias JIRA-CA -importcert  -file ../JIRA-CA.crt`
4. Start the app with java -jar branchcreator-<version-nr>-spring-boot.jar 

## Logging
The app use log4j2. It's configured to log to `./branchcreator.log`.
You can define an environment variable `LOG_DIR`  to change the directory,
or with lesser preference set `-Dlog.dir` 
 
## Known Bugs
 
## Icons
https://icons8.com/icon/set/branch/wired MergeGit -> Cute Outline 