package de.ppi.oss.gitbranchcreator.config

import org.apache.logging.log4j.LogManager
import java.io.File
import java.io.FileReader
import java.util.*

/**
 * Comfort-object to access the application.properties.
 */
object ApplicationProperties {

    private val logger = LogManager.getLogger(ApplicationProperties::class.java)

    private val properties = readProperties()

    fun getJiraUrl(): String {
        return properties.getProperty("jira.url")
    }

    fun getJiraUser(): String {
        return properties.getProperty("jira.user")
    }

    fun getJiraPassword(): String? {
        return properties.getProperty("jira.password")
    }


    fun getJiraJql(): String {
        return properties.getProperty("jira.jql")
    }

    fun getGitCommand(): String {
        return properties.getProperty("git.cmd")
    }

    fun getGitRepository(): String {
        return properties.getProperty("git.repopath")
    }

    fun getGitBranchPattern(): String {
        return properties.getProperty("git.branchnamepattern")
    }

    private fun readProperties(): Properties {
        val properties = java.util.Properties()
        val propertiesFileName = "application.properties"
        var propertiesFile = File(propertiesFileName)
        if (!propertiesFile.exists()) {
            val loadClass = try {
                // First try if is a Sprint-Boot-Runner-jar.
                ClassLoader.getSystemClassLoader().loadClass("org.springframework.boot.loader.Launcher")
            } catch (e: ClassNotFoundException) {
                ApplicationProperties::class.java
            }
            val location = loadClass.protectionDomain.codeSource.location
            val currentLocation = File(location.toURI())
            propertiesFile = if (currentLocation.isDirectory) {
                File(currentLocation, propertiesFileName)
            } else {
                File(currentLocation.parent, propertiesFileName)
            }
        }

        if (propertiesFile.exists()) {
            logger.info("Load properties from : ${propertiesFile.absolutePath}")
        } else {
            logger.error("Can't load properties from : ${propertiesFile.absolutePath}")
            System.exit(1)
        }
        properties.load(FileReader(propertiesFile))
        return properties
    }
}