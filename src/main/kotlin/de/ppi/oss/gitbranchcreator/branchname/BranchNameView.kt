package de.ppi.oss.gitbranchcreator.branchname

import de.ppi.oss.gitbranchcreator.BranchCreatorDataModel
import de.ppi.oss.gitbranchcreator.jira.JiraIssue
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.layout.Priority
import tornadofx.*

/**
 * View to define the new branch-name.
 */
class BranchNameView : View("Branch Name") {

    private val controller: BranchNameController by inject()
    private val branchCreatorModel: BranchCreatorDataModel by inject()
    private val filteredJiraIssus = mutableListOf<JiraIssue>().observable()
    private val allJiraIssue = branchCreatorModel.jiraTicketsProperty

    private var filterTextfield: TextField by singleAssign()

    override val root = borderpane {
        top {
            hbox {
                filterTextfield = textfield {
                    promptText = "Filter"
                    textProperty().addListener { _, oldVal, newVal -> filter(oldVal, newVal) }
                }
                button("Load Issues") {
                    action { reloadIssues() }
                }
            }
        }
        center {
            tableview(filteredJiraIssus) {
                readonlyColumn("Key", JiraIssue::key).weightedWidth(weight = 1.0, minContentWidth = true, padding = 5.0)
                readonlyColumn("Summary", JiraIssue::summary).weightedWidth(50)
                selectionModel.selectedItemProperty().addListener { _, _: JiraIssue?, newValue: JiraIssue? ->
                    if (newValue != null) {
                        controller.selectedJiraIssueChanged(newValue)
                    }
                }
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
                vboxConstraints { vGrow = Priority.ALWAYS }
                smartResize()
            }
        }
        bottom {
            form {
                fieldset {
                    field("Branchname") {
                        textfield {
                            promptText = "Branchname"
                            textProperty().bindBidirectional(branchCreatorModel.branchName)
                        }
                    }
                }
            }
        }
        allJiraIssue.addListener { _ -> filter(null, filterTextfield.text) }
        controller.loadIssues()
    }


    private fun filter(oldVal: String?, newVal: String) {
        val searchAbleList = if (oldVal == null || newVal.length < oldVal.length) {
            allJiraIssue.value.toMutableList()
        } else {
            filteredJiraIssus.toMutableList()
        }
        val value = newVal.toUpperCase()
        if (value.isEmpty()) {
            filteredJiraIssus.setAll(searchAbleList)
        } else {
            val currentEntries = mutableListOf<JiraIssue>()
            for (entry in searchAbleList) {
                if (entry.key.contains(value) || entry.summary.contains(value)) {
                    currentEntries.add(entry)
                }
            }
            filteredJiraIssus.setAll(currentEntries)
        }
    }

    private fun reloadIssues() {
        if (!controller.isJiraPasswordSet()) {
            dialog("Enter jira password") {
                val model = ViewModel()
                val password = model.bind { SimpleStringProperty() }
                val okButton = button("Load-Issues") {
                    this.setDefaultButton(true)
                    action {
                        model.commit {
                            controller.setJiraPassword(password.value)
                            this@dialog.close()
                            controller.loadIssues()
                        }
                    }
                }
                field("Password") {
                    passwordfield(password) {
                        required()
                        whenDocked { requestFocus() }
                    }
                }
                buttonbar {
                    add(okButton)
                }
            }
        } else {
            controller.loadIssues()
        }
    }


}
