package de.ppi.oss.gitbranchcreator.branchname

import de.ppi.oss.gitbranchcreator.BranchCreatorDataModel
import de.ppi.oss.gitbranchcreator.config.ApplicationProperties
import de.ppi.oss.gitbranchcreator.jira.JiraIssue
import de.ppi.oss.gitbranchcreator.jira.JiraSimple
import tornadofx.*
import java.text.MessageFormat

class BranchNameController : Controller() {

    private val branchCreatorModel: BranchCreatorDataModel by inject()
    private val re = Regex("[^a-zA-Z0-9_-]")
    private val jiraClient = JiraSimple(ApplicationProperties.getJiraUrl(), ApplicationProperties.getJiraJql(), ApplicationProperties.getJiraUser(), ApplicationProperties.getJiraPassword())

    fun loadIssues() {
        branchCreatorModel.jiraTickets = jiraClient.getIssues()
    }

    fun isJiraPasswordSet(): Boolean {
        return jiraClient.isPasswordSet()
    }

    fun setJiraPassword(password: String?) {
        jiraClient.setPassword(password)
    }

    fun selectedJiraIssueChanged(issue: JiraIssue) {
        var branchName = MessageFormat.format(ApplicationProperties.getGitBranchPattern(), issue.key, issue.summary)
        branchName = branchName.replace(' ', '_')
        branchName = branchName.replace("ä", "ae")
        branchName = branchName.replace("ö", "oe")
        branchName = branchName.replace("ü", "ue")
        branchName = re.replace(branchName, "")
        branchCreatorModel.branchName.value = branchName

    }

}