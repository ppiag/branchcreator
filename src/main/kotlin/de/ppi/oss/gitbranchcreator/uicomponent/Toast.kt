package de.ppi.oss.gitbranchcreator.uicomponent

import javafx.animation.KeyFrame
import javafx.animation.KeyValue
import javafx.animation.Timeline
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.util.Duration

/**
 * Helper class to show for a short time a message.
 */
object Toast {
    /**
     * Shows the given message for the defined time.
     * @param ownerStage the currentStage
     * @param message the message which should be shown.
     * @param duration the time in ms how long the message should be shown. Default 1000.
     * @param fadeInDelay Milliseconds for fade in the message. Default 100.
     * @param fadeInDelay Milliseconds for fade out the message. Default 100.
     */
    fun showToastMessage(ownerStage: Stage, message: String, duration: Int = 1000, fadeInDelay: Int = 100, fadeOutDelay: Int = 100) {
        val toastStage = Stage()
        toastStage.initOwner(ownerStage)
        toastStage.isResizable = false
        toastStage.initStyle(StageStyle.TRANSPARENT)

        val text = Text(message)
        text.font = Font.font("Verdana", 40.0)
        text.fill = Color.RED

        val root = StackPane(text)
        root.style = "-fx-background-radius: 20; -fx-background-color: rgba(0, 0, 0, 0.2); -fx-padding: 50px;"
        root.opacity = 0.0

        val scene = Scene(root)
        scene.fill = Color.TRANSPARENT
        toastStage.scene = scene
        toastStage.show()

        val fadeInTimeline = Timeline()
        val fadeInKey1 = KeyFrame(Duration.millis(fadeInDelay.toDouble()), KeyValue(toastStage.scene.root.opacityProperty(), 1))
        fadeInTimeline.keyFrames.add(fadeInKey1)
        fadeInTimeline.setOnFinished { ae ->
            Thread {
                try {
                    Thread.sleep(duration.toLong())
                } catch (e: InterruptedException) {
                    // Ignore
                }

                val fadeOutTimeline = Timeline()
                val fadeOutKey1 = KeyFrame(Duration.millis(fadeOutDelay.toDouble()), KeyValue(toastStage.scene.root.opacityProperty(), 0))
                fadeOutTimeline.keyFrames.add(fadeOutKey1)
                fadeOutTimeline.setOnFinished { aeb -> toastStage.close() }
                fadeOutTimeline.play()
            }.start()
        }
        fadeInTimeline.play()
    }
}