package de.ppi.oss.gitbranchcreator.uicomponent

import javafx.beans.property.SimpleObjectProperty
import javafx.scene.control.ListView
import javafx.scene.control.SelectionMode
import javafx.scene.control.TextField
import tornadofx.*

/**
 * List view which show all Entries where toString contains the search string.
 */
class FilteringList<T>(private val allEntries: SimpleObjectProperty<List<T>>,
                       private val minHeightParam: Double = 20.0,
                       private val maxHeightParam: Double = 180.0,
                       private val selectionMode: SelectionMode = SelectionMode.MULTIPLE) : Fragment() {

    private var filterTextfield: TextField by singleAssign()
    private var listView: ListView<T> by singleAssign()

    init {
        allEntries.addListener { _ -> search(null, filterTextfield.text) }
    }

    override val root = vbox {
        filterTextfield = textfield {
            promptText = "Filter"
            textProperty().addListener { _, oldVal, newVal -> search(oldVal, newVal) }
        }
        listView = listview(allEntries.value.toMutableList().observable()) {
            minHeight = minHeightParam
            maxHeight = maxHeightParam
            selectionModel.selectionMode = selectionMode
        }
    }

    private fun search(oldVal: String?, newVal: String) {
        val searchAbleList = if (oldVal == null || newVal.length < oldVal.length) {
            allEntries.value.toMutableList()
        } else {
            listView.items
        }
        val value = newVal.toUpperCase()
        if (value.isEmpty()) {
            listView.items.setAll(searchAbleList)
        } else {
            val currentEntries = mutableListOf<T>()
            for (entry in searchAbleList) {
                if (entry.toString().toUpperCase().contains(value)) {
                    currentEntries.add(entry)
                }
            }
            listView.items.setAll(currentEntries)
        }
    }

    fun getSelectedItems(): List<T> {
        return listView.selectionModel.selectedItems
    }
}