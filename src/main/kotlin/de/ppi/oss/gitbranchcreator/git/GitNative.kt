package de.ppi.oss.gitbranchcreator.git

import de.ppi.oss.gitbranchcreator.config.ApplicationProperties
import org.apache.logging.log4j.LogManager
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit
import kotlin.streams.toList

/**
 * Native implementation of the GIT-Command.
 */
object GitNative : Git {
    private val logger = LogManager.getLogger(GitNative::class.java)
    override fun getAllBranches(): List<String> {
        logger.debug("Fetch information ")
        runGitCommand(listOf(ApplicationProperties.getGitCommand(), "fetch", "-p"))
        logger.debug("Update branch list")
        val command = listOf(ApplicationProperties.getGitCommand(), "branch", "--all")
        val allOutputLines = runGitCommand(command).split("\n").stream()
        logger.debug("Prepare output")
        val branchNames = allOutputLines.filter { branch -> branch.length > 2 }
        /*.sorted { o1, o2 ->
            if (o1.startsWith("*")) {
                -1
            } else if (o2.startsWith("*")) {
                1
            } else if (o2.startsWith("  remotes") && !o1.startsWith("  remotes")) {
                -1
            } else if (o1.startsWith("  remotes") && !o2.startsWith("  remotes")) {
                1
            } else {
                o1.compareTo(o2)
            }
            }*/
        val listOfBranches = branchNames.map { branch -> branch.substring(2) }.toList()
        logger.debug("Return list of size ${listOfBranches.size}")
        return listOfBranches
    }

    override fun createBranch(name: String, baseBranches: Array<String>): String {
        val command = mutableListOf(ApplicationProperties.getGitCommand(), "checkout", "-b", name)
        when {
            baseBranches.size == 1 -> command.addAll(listOf("--no-track", baseBranches[0]))
            baseBranches.size > 1 -> command.add(findMergeBase(baseBranches))
        }
        return runGitCommand(command)
    }

    private fun findMergeBase(baseBranches: Array<String>): String {
        if (baseBranches.size <= 1) {
            throw IllegalArgumentException("baseBranches has lesser than 2 arguments.")
        }
        val command = mutableListOf(ApplicationProperties.getGitCommand(), "merge-base")
        command.addAll(baseBranches)
        return runGitCommand(command).trim('\r').trim('\n').trim()
    }

    private fun runGitCommand(commandArguments: List<String>): String {
        val gitRepo = File(ApplicationProperties.getGitRepository())
        if (!gitRepo.exists()) {
            throw IllegalStateException("${gitRepo.absolutePath} does not exists")
        }

        try {
            val proc = ProcessBuilder(*commandArguments.toTypedArray())
                    .directory(gitRepo)
                    .redirectOutput(ProcessBuilder.Redirect.PIPE)
                    .redirectError(ProcessBuilder.Redirect.PIPE)
                    .start()
            val output = proc.inputStream.bufferedReader().readText()
            val error = proc.errorStream.bufferedReader().readText()
            // TODO This isn't really useful, because this line will only reached if process is finished.
            // The proc must put into a future on a own thread, than we can cancel it.
            proc.waitFor(60, TimeUnit.SECONDS)

            if (proc.exitValue() != 0) {
                throw Git.GitProcessFailedException("In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< failed with exitCode ${proc.exitValue()} and output >$output< or error >$error<.")
            }
//            println("In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< failed with exitCode ${proc.exitValue()} and output >$output< or error >$error<.")
            logger.info("In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< produced following output\n${output}")
            return output
        } catch (e: IOException) {
            throw Git.GitProcessFailedException("Command >${commandArguments.joinToString(" ")}< failed with Exception ${e.message}", e); }
    }

}