package de.ppi.oss.gitbranchcreator.git

import de.ppi.oss.gitbranchcreator.git.Git.GitProcessFailedException

/**
 * Service to provide some git operations, all could throw an [GitProcessFailedException].
 */
interface Git {

    /**
     * Returns a list of all existing branches and fetch the remote state.
     */
    fun getAllBranches(): List<String>

    /**
     * Creates a new branch, which have a mergebase of all given branches.
     */
    fun createBranch(name: String, baseBranches: Array<String>): String

    class GitProcessFailedException : RuntimeException {
        constructor(message: String, ex: Exception) : super(message, ex)
        constructor(message: String) : super(message)
    }

}