package de.ppi.oss.gitbranchcreator.branchcreation

import de.ppi.oss.gitbranchcreator.BranchCreatorDataModel
import de.ppi.oss.gitbranchcreator.git.Git
import de.ppi.oss.gitbranchcreator.git.GitNative
import org.apache.logging.log4j.LogManager
import tornadofx.*

/**
 * Controller to to create the branch.
 */
class BranchCreationController : Controller() {
    private val logger = LogManager.getLogger(BranchCreationController::class.java)

    private val branchCreatorModel: BranchCreatorDataModel by inject()

    private val git = GitNative

    init {
    }

    fun createBranch() {
        try {
            val output = git.createBranch(branchCreatorModel.branchName.value, branchCreatorModel.baseBranches.toTypedArray())
            branchCreatorModel.addLoginfo(output)
        } catch (e: Git.GitProcessFailedException) {
            logger.error("Error creating branch.", e)
            branchCreatorModel.addLoginfo("Error creating branch. ", e)
        }
        branchCreatorModel.resetExistingBranches(git.getAllBranches())
    }
}