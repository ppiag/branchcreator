package de.ppi.oss.gitbranchcreator.branchcreation

import de.ppi.oss.gitbranchcreator.BranchCreatorDataModel
import de.ppi.oss.gitbranchcreator.Message
import de.ppi.oss.gitbranchcreator.uicomponent.Toast
import javafx.beans.binding.Bindings
import javafx.geometry.Orientation.VERTICAL
import javafx.scene.control.ButtonBar
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.input.Clipboard
import javafx.scene.input.MouseButton
import javafx.scene.layout.Priority
import javafx.util.Callback
import org.apache.commons.lang.StringUtils
import tornadofx.*
import java.util.concurrent.Callable


/**
 * View for creation the branch, shows the messages.
 */
class BranchCreationView : View("Branch Creation") {

    private val controller = find(BranchCreationController::class)
    private val branchCreatorModel: BranchCreatorDataModel by inject()

    override val root = vbox {
        form {
            fieldset(labelPosition = VERTICAL) {
                buttonbar {
                    button(text = "Create", type = ButtonBar.ButtonData.LEFT) {
                        action {
                            controller.createBranch()
                        }
                        disableProperty().bind(Bindings.createBooleanBinding(Callable {
                            val branchname = branchCreatorModel.branchName.value
                            StringUtils.isBlank(branchname) || branchCreatorModel.possibleBranches.contains(branchname)
                                    || branchCreatorModel.baseBranches.contains(branchname)
                        }, branchCreatorModel.baseBranchesProperty, branchCreatorModel.possibleBranchesProperty, branchCreatorModel.branchName))
                    }

                }
                label("Logmessages. Double-Click to copy message to clipboard.")
                tableview(branchCreatorModel.logMessages) {
                    readonlyColumn("Time", Message::time).weightedWidth(weight = 1.0, minContentWidth = true, padding = 5.0)
                    readonlyColumn("Message", Message::fullMessage).weightedWidth(50)
                    rowFactory = Callback<TableView<Message>, TableRow<Message>> {
                        val row = TableRow<Message>()
                        row.setOnMouseClicked { event ->
                            if (!row.isEmpty && event.button == MouseButton.PRIMARY
                                    && event.clickCount == 2) {
                                val clickedRow = row.item
                                Clipboard.getSystemClipboard().putString(clickedRow.fullMessage)
                                Toast.showToastMessage(this@BranchCreationView.primaryStage, "Message copied to clipboard", 1000)
                            }
                        }
                        row
                    }
                    columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
                    hboxConstraints { hGrow = Priority.ALWAYS }
                    smartResize()
                }
            }
        }
    }
}
