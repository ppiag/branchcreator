package de.ppi.oss.gitbranchcreator

import de.ppi.oss.gitbranchcreator.jira.JiraIssue
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

/**
 * [ViewModel] for [BranchCreatorData].
 */
class BranchCreatorDataModel : ViewModel() {
    private val branchCreatorData = BranchCreatorData()
    private var existingBranches = emptyList<String>()
    val possibleBranchesProperty = SimpleObjectProperty<List<String>>(emptyList())
    var possibleBranches by this.possibleBranchesProperty

    val branchName = this.bind(true) { this.branchCreatorData.branchNameProperty }

    val baseBranchesProperty = SimpleObjectProperty<List<String>>(emptyList())
    var baseBranches by this.baseBranchesProperty
    val jiraTicketsProperty = SimpleObjectProperty<List<JiraIssue>>(emptyList<JiraIssue>())
    var jiraTickets by this.jiraTicketsProperty

    val logMessages = SimpleListProperty<Message>(mutableListOf<Message>().observable())

    /** Calc the possible branches .*/
    fun resetPossibleBranches() {
        val possibleBranchesTmp = this.existingBranches.toMutableList()
        possibleBranchesTmp.removeAll(this.baseBranches)
        this.possibleBranches = possibleBranchesTmp.toList()
    }

    /** Set the list of existing branches. */
    fun resetExistingBranches(allBranches: List<String>) {
        this.existingBranches = allBranches
        this.resetPossibleBranches()
    }

    fun addLoginfo(output: String, exception: Exception? = null) {
        logMessages.add(0, Message(output, exception))
        while (logMessages.size > 300) {
            logMessages.removeAt(300)
        }
    }
}

/**
 * Data which are needed to create a new branch.
 */
class BranchCreatorData() {
    val branchNameProperty = SimpleStringProperty()
    var branchName by this.branchNameProperty

    val logMessagesProperty = SimpleListProperty<Message>(mutableListOf<Message>().observable())
    var logMessages by this.logMessagesProperty


}
