package de.ppi.oss.gitbranchcreator.jira

/**
 * Container for the Jira_issue.
 */
data class JiraIssue(val key: String, val summary: String) {
}
