package de.ppi.oss.gitbranchcreator.jira


import net.rcarz.jiraclient.BasicCredentials
import net.rcarz.jiraclient.JiraClient
import net.rcarz.jiraclient.JiraException
import org.apache.logging.log4j.LogManager
import java.io.File

const val CSV_SEPARATOR = ";"

/**
 * Implementierung based on https://github.com/rcarz/jira-client.
 * The client is old, but popular and the atlassian documentation is a little bit difficult.
 */
class JiraSimple(private val jiraurl: String, private val jql: String, private val username: String, private var password: String?) : Jira {
    private val logger = LogManager.getLogger(JiraSimple::class.java)
    override fun getIssues(): List<JiraIssue> {
        val creds = BasicCredentials(username, password)
        val jira = JiraClient(jiraurl, creds)

        val cacheDir = File(".cache")
        val cache = File(cacheDir, "jiraIssues.csv")
        if (!cacheDir.exists()) {
            cacheDir.mkdirs()
        }
        cache.createNewFile()
        if (password != null) {
            try {
                val searchResult = jira.searchIssues(jql, "key, summary")
                val onlineTickets = searchResult.issues.map { issue -> JiraIssue(issue.key, issue.summary) }
                if (!cache.createNewFile()) {
                    cache.delete()
                    cache.createNewFile()
                }

                cache.bufferedWriter().use { out ->
                    onlineTickets.forEach { issue ->
                        out.write("${issue.key}$CSV_SEPARATOR${issue.summary}")
                        out.newLine()
                    }
                }
                return onlineTickets
            } catch (e: JiraException) {
                logger.error("Can't load jira-issues. Use cache.", e.printStackTrace())
                // Load from Cache
                password = null
            }
        }

        val lines = cache.useLines { lines -> lines.toList() }
        return lines.map { line ->
            val parts = line.split(CSV_SEPARATOR)
            val summary = parts.slice(IntRange(1, parts.size - 1)).joinToString(CSV_SEPARATOR)
            JiraIssue(parts[0], summary)
        }
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun isPasswordSet(): Boolean {
        return this.password != null
    }
}
