package de.ppi.oss.gitbranchcreator.jira

interface Jira {

    /**
     * Returns all issues related to the jql.
     */
    fun getIssues(): List<JiraIssue>
}