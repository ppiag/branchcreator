package de.ppi.oss.gitbranchcreator

import javafx.scene.image.Image
import javafx.stage.Screen
import javafx.stage.Stage
import tornadofx.*

/**
 * App to create a branchname via issue and with arbitrary base-branches.
 */
class BranchCreatorApp : App(BranchCreatorView::class) {

    object Companion {
        @JvmStatic
        fun main(args: Array<String>) {
            launch<BranchCreatorApp>(args)
        }
    }

    override fun start(stage: Stage) {
        super.start(stage)
        stage.icons.add(Image(BranchCreatorApp::class.java.getResourceAsStream("/icons8-merge-git-64.png")))
        val primaryScreenBounds = Screen.getPrimary().visualBounds

        //set Stage boundaries to visible bounds of the main screen
        stage.x = primaryScreenBounds.minX + 100.0
        stage.y = primaryScreenBounds.minY + 10.0
        //stage.width = primaryScreenBounds.width
        stage.height = primaryScreenBounds.height - 20.0
    }
}