package de.ppi.oss.gitbranchcreator

import de.ppi.oss.gitbranchcreator.basebranches.BaseBranchView
import de.ppi.oss.gitbranchcreator.branchcreation.BranchCreationView
import de.ppi.oss.gitbranchcreator.branchname.BranchNameView
import tornadofx.*

/**
 * Main-View of the branch-creator-app.
 */
class BranchCreatorView : View("Branch Creator") {
    val branchNameView = find(BranchNameView::class)
    val baseBranchView = find(BaseBranchView::class)
    val branchCreationView = find(BranchCreationView::class)
    override val root = gridpane {
        row {
            add(branchNameView)
        }
        row {
            add(baseBranchView)
        }
        row {
            add(branchCreationView)
        }
        constraintsForRow(0).percentHeight = 60.0
        constraintsForRow(1).percentHeight = 40.0
        constraintsForRow(2).percentHeight = 70.0
        constraintsForRow(2).fillHeightProperty().value = true
    }

}
