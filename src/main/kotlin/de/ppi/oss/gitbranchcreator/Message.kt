package de.ppi.oss.gitbranchcreator

import org.apache.commons.lang.exception.ExceptionUtils
import java.time.LocalDateTime

data class Message(val message: String, val exception: Exception? = null) {

    val time = LocalDateTime.now()

    val fullMessage = if (exception == null) {
        message
    } else {
        message + "\n" + ExceptionUtils.getFullStackTrace(exception)
    }

}