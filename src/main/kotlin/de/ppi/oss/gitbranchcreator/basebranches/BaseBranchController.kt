package de.ppi.oss.gitbranchcreator.basebranches

import de.ppi.oss.gitbranchcreator.BranchCreatorDataModel
import de.ppi.oss.gitbranchcreator.git.GitNative
import tornadofx.*

/**
 * Controller to find all branches where the new branch based from.
 */
class BaseBranchController : Controller() {

    private val branchCreatorModel: BranchCreatorDataModel by inject()

    private val git = GitNative

    init {
        reload()
    }

    fun reload() {
        branchCreatorModel.resetExistingBranches(git.getAllBranches())
    }

    fun unchooseBaseBranches(oldBaseBranches: List<String>) {
        val newBaseBranches = branchCreatorModel.baseBranches.toMutableList()
        newBaseBranches.removeAll(oldBaseBranches)
        branchCreatorModel.baseBranches = newBaseBranches
        branchCreatorModel.resetPossibleBranches()
    }

    fun chooseBaseBranches(newBaseBranches: List<String>) {
        val newBaseBranchesList = branchCreatorModel.baseBranches.toMutableList()
        newBaseBranchesList.addAll(newBaseBranches)
        branchCreatorModel.baseBranches = newBaseBranchesList
        branchCreatorModel.resetPossibleBranches()
    }

}
