package de.ppi.oss.gitbranchcreator.basebranches

import de.ppi.oss.gitbranchcreator.BranchCreatorDataModel
import de.ppi.oss.gitbranchcreator.uicomponent.FilteringList
import javafx.geometry.Pos
import javafx.scene.control.SelectionMode
import tornadofx.*

/**
 * View to choose base branches.
 */
class BaseBranchView : View("Base Branches") {
    private val controller = find(BaseBranchController::class)
    private val branchCreatorModel: BranchCreatorDataModel by inject()

    private val possibleBranchList = FilteringList(branchCreatorModel.possibleBranchesProperty,
            minHeightParam = 20.0, maxHeightParam = 180.0, selectionMode = SelectionMode.MULTIPLE)
    private val baseBranchList = FilteringList(branchCreatorModel.baseBranchesProperty,
            minHeightParam = 20.0, maxHeightParam = 180.0, selectionMode = SelectionMode.MULTIPLE)

    override val root = vbox(10.0, alignment = Pos.TOP_CENTER) {
        label("Base-branches in which the new branch should be merged.")
        hbox(10.0, alignment = Pos.BASELINE_CENTER) {
            add(possibleBranchList)

            vbox {
                alignment = Pos.BASELINE_CENTER
                button(">>") {
                    action {
                        controller.chooseBaseBranches(possibleBranchList.getSelectedItems())
                    }
                }
                button("<<") {
                    action { controller.unchooseBaseBranches(baseBranchList.getSelectedItems()) }

                }
                button("Reload") {
                    action { controller.reload() }
                }
            }

            add(baseBranchList)
        }
    }

}
